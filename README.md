# wotimer-darebee-config

Configuration file for wotimer using [Darebee](https://darebee.com)© workouts specifications.<br>
Those are under the following license.<br>
The rest is AGPLv3 or later.

# Instructions

1- Download and import it directly in the app using the button.

2- Clone the repo and copy `projects.json` in `~/.local/share/realroot/wotimer/`. Or import the file in the app.<br/>
Until version `0.2.0` it was `wotimer.json` in `~/.config/realroot`

3- Run this command (it will overwrite the file):
```
mkdir -p ~/.config/realroot && wget -O ~/.local/share/realroot/wotimer/projects.json https://codeberg.org/realroot/wotimer-darebee-config/raw/branch/main/projects.json
```

# License

[Terms of Use](https://darebee.com/terms-of-use.html)

All of the materials on this resource (https://darebee.com) are released under [Creative Commons Attribution - Non Commercial - No Derivatives International license](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode).

You can share any of the DAREBEE materials online or offline so long as the posters are not modified in any way, nothing is added or removed, and our copyright is kept intact and is clearly visible. You can use individual workouts, programs and challenges on your website, your blog, your group and/or Facebook page, as part of promoting our mission to make fitness a right and not a privilege as part of a review or when documenting your own fitness journey - it’s perfectly cool with us if you do. 

We will appreciate it if you mention where you got them from and link back to the source. DAREBEE is a trademarked, registered non-profit; we rely exclusively on user-donations to stay up and continue our work so every time you mention us - it helps.

Naturally, we don’t allow for any of our materials to be sold or used - individually or as part of a package. This includes books, ebooks & mobile apps.

You can distribute our work individually, as-is, but only if it’s done free of charge and for non-commercial purposes. You can print out the posters and give them away to help raise awareness of health and fitness, for example, so long as you don’t charge for them. 

All exercise illustrations used in the DAREBEE materials are unique, exclusive to us and protected by copyright. They cannot be cropped out and/or used anywhere else outside of the DAREBEE posters. 

None of our materials can be used as part of a product (even if it's free) e.g., ebooks, books or mobile apps. 

If you are a gym owner, a physiotherapist, a doctor or a personal trainer you are free to use our materials in your sessions or to give them away to your clients. The same rules apply: the materials have to stay intact, unmodified, and they have to be given away for free. We don’t mind when our work is used to help people (we are thrilled when it is!), so long as it’s not done for profit. 

If you have any questions or not sure about something you can contact us.

If you think someone is abusing our work and our trust, please, do let us know.  

With thanks,
The DAREBEE team
