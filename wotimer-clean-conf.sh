#!/bin/sh

# Check if the first argument (input file) is provided
if [ -z "$1" ]; then
  echo "Usage: $0 <input.json> [output.json]"
  exit 1
fi

# Assign input and output file variables
input_file="$1"
output_file="${2:-output.json}"  # Default to output.json if no second argument is provided

jq '.projects[] |= if . then (.description = "" | .map = "" | .pdf = "") else . end' "$input_file" > "$output_file"

#echo "Processed JSON saved to $output_file"

mv $output_file projects.json
